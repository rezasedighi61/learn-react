import "./App.css";
import React, { useEffect } from "react";
import AddTodo from "./AddTodo";
import Todos from "./Todos";
import Playground from "./Playground";
import LanguaeProvider from "./MultiLanguageProvider";

// const getTodos = () => {
//   fetch("https://jsonplaceholder.typicode.com/todos").then((response) => {
//     response.json().then((res) => {
//       fetch("https://jsonplaceholder.typicode.com/todos/" + res[0].id)
//         .then((res) => res.json())
//         .then((res) => console.log(res));
//     });
//   });
// };

// const asyncAwaitVersion = async () => {
//   try {
//     const rawData = await fetch("https://jsonplaceholder.typicode.com/todos");
//     const jsonData = await rawData.json();
//     const Rawresult = await fetch(
//       "https://jsonplaceholder.typicode.com/todos/" + jsonData[0].id
//     );
//     const data = await Rawresult.json();
//   } catch (error) {
//     console.log(error);
//   }
//   console.log("🚀 ~ file: App.js ~ line 24 ~ asyncAwaitVersion ~ data", data);
// };

function App() {
  useEffect(() => {
    // const userData = [
    //   { name: "reza", score: [20, 15, 10, 5] },
    //   { name: "ali", score: [10, 15, 12, 5, 16] },
    // ];
    // console.log(
    //   userData.reduce((prev, current) => {
    //     prev[current.name] =
    //       current.score.reduce((prev, current) => prev + current, 0) /
    //       current.score.length;
    //     return prev;
    //   }, {})
    // );
    // asyncAwaitVersion();
  }, []);

  return (
    <>
      <LanguaeProvider>
        <Playground />
      </LanguaeProvider>
      {/* <Todos /> */}
    </>
  );
}

export default App;
