import axios from "axios";
import {
  createContext,
  forwardRef,
  useContext,
  useEffect,
  useReducer,
  useRef,
} from "react";
import { useCallback } from "react";
import { useMemo, useState } from "react";
import { SetLangContext, TitleContext } from "./MultiLanguageProvider";
import Todos from "./Todos";

const Link = ({ to, children }) => {
  const click = (e) => {
    e.preventDefault();
    window.history.pushState({}, null, to);
  };

  return (
    <a href={to} onClick={click}>
      {children}
    </a>
  );
};
const SearchNpmPackage = () => {
  const [search, setSearch] = useState("");
  const [loading, toggleLoading] = useReducer((prev) => !prev, false);
  const [packages, setPackages] = useState(null);

  const submit = async () => {
    toggleLoading();
    setPackages(null);
    const { data } = await axios.get(
      `https://www.npmjs.com/search/suggestions?q=${search}`
    );

    setPackages(data);
    toggleLoading();
  };

  return (
    <div>
      <div>
        <input value={search} onChange={(e) => setSearch(e.target.value)} />
        <button onClick={submit} disabled={loading}>
          search
        </button>
        <Link to="/todo">go to todos</Link>
      </div>
      {loading && <p>fetching packages</p>}
      {Array.isArray(packages) && !packages.length && <p>no result found!</p>}
      {packages && (
        <ul>
          {packages.map((item) => (
            <li key={item.name}>
              <p>{item.name}</p>
              <p>{item.version}</p>
              <p>{item.description}</p>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

const Route = ({ component: Component, path, exact }) => {
  const pathname = window.location.pathname;
  const matchPath = exact ? pathname === path : pathname.startsWith(path);

  return matchPath ? <Component /> : null;
};

const Playground = () => {
  const ref = useRef();
  useEffect(() => {
    console.log(ref.current);
  }, []);
  return (
    <>
      <Route component={SearchNpmPackage} path="/search" exact />
      <Route component={Todos} path="/todo" />
    </>
  );
};

export default Playground;
