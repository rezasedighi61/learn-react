import { useState } from "react";

const TodoItem = ({ todo, toggleTodo, deleteTodo, editTodo }) => {
  const [newName, setNewName] = useState(todo.name);
  const [isEditingTodo, setIsEditting] = useState(false);

  const toggleEdditing = () => setIsEditting(!isEditingTodo);

  const onChange = (e) => setNewName(e.target.value);

  const submitEdit = () => {
    editTodo(todo.id, newName);
    setIsEditting(false);
  };

  return (
    <li style={{ textDecoration: todo.done ? "line-through" : "unset" }}>
      {isEditingTodo ? (
        <input onChange={onChange} value={newName} />
      ) : (
        <span onClick={() => toggleTodo(todo.id)}>{todo.name}</span>
      )}
      <button onClick={() => deleteTodo(todo.id)}>DEL</button>
      {isEditingTodo ? (
        <button onClick={submitEdit}>Save</button>
      ) : (
        <button onClick={toggleEdditing}>Edit</button>
      )}
    </li>
  );
};

export default TodoItem;
