import { useEffect, useRef, useState } from "react";
import AddTodo from "./AddTodo";
import TodoFilters from "./TodoFilters";
import TodoItem from "./TodoItem";

let id = 1;

const Todos = () => {
  const [todos, setTodos] = useState(
    JSON.parse(window.localStorage.getItem("todos") || "[]")
  );
  const [filter, setFilter] = useState("all");

  const ulElement = useRef(1);

  const handleAddTodos = (todoName) => {
    setTodos((prev) => [...prev, { name: todoName, id: id++, done: false }]);
  };

  const toggleTodo = (id) => {
    setTodos((prev) =>
      prev.map((todo) =>
        todo.id === id ? { ...todo, done: !todo.done } : todo
      )
    );
  };

  const deleteTodo = (id) => {
    setTodos((prev) => prev.filter((todo) => todo.id !== id));
  };

  const editTodo = (id, newTitle) => {
    setTodos((prev) =>
      prev.map((todo) => (todo.id === id ? { ...todo, name: newTitle } : todo))
    );
  };

  const filteredTodos = () => {
    switch (filter) {
      case "all":
        return todos;
      case "done":
        return todos.filter((todo) => todo.done);
      case "undone":
        return todos.filter((todo) => !todo.done);
      default:
        return "";
    }
  };

  useEffect(() => {
    window.localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  // useEffect(() => {
  //   console.log(todos);
  // }, [filter, todos]);

  return (
    <>
      <TodoFilters selectedFilter={filter} setFilter={setFilter} />
      <br />
      <AddTodo handleAddTodo={handleAddTodos} />
      <ul>
        {filteredTodos().map((todo) => (
          <TodoItem
            key={todo.id}
            toggleTodo={toggleTodo}
            deleteTodo={deleteTodo}
            editTodo={editTodo}
            todo={todo}
          />
        ))}
      </ul>
    </>
  );
};

export default Todos;
