const filters = [
  { label: "all", id: "all" },
  { label: "done", id: "done" },
  { label: "undone", id: "undone" },
];

const TodoFilters = ({ setFilter, selectedFilter }) => {
  return (
    <div>
      {filters.map((filter) => (
        <button
          onClick={() => setFilter(filter.id)}
          style={{ background: filter.id === selectedFilter ? "grey" : "#fff" }}
        >
          {filter.label}
        </button>
      ))}
    </div>
  );
};

export default TodoFilters;
