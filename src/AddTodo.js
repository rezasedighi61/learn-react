import { useState } from "react";

const AddTodo = ({ handleAddTodo }) => {
  const [inputValue, setInputValue] = useState("");

  const onAdd = () => {
    handleAddTodo(inputValue);
    setInputValue("");
  };

  const onChange = (e) => {
    setInputValue(e.target.value);
  };

  // const increaseCounter = () => {
  //   setCounter((prev) => prev + 1);
  //   setCounter((prev) => prev + 1);

  //   // setCounter((prev) => prev + 1);
  // };

  return (
    <div>
      <input value={inputValue} onChange={onChange} />
      {/* <p>your todo name is {inputValue || "empty"}</p>
      {!inputValue ? <p>todo name is required</p> : null}
      {!inputValue &&<p>todo name is required</p>} */}
      <button onClick={onAdd}>add Todo</button>
    </div>
  );
};

export default AddTodo;
