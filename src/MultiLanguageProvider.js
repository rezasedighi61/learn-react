import { createContext, useMemo, useReducer, useState } from "react";

export const TitleContext = createContext();
export const SetLangContext = createContext();

const reducer = (prevState, action) => {
  switch (action.type) {
    case "setLang":
      return { ...prevState, lang: action.payload };
    case "setTimeZone":
      return { ...prevState, timeZone: action.payload };
    default:
      return {};
  }
};

const LanguaeProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, { lang: "en", timeZone: "us" });
  console.log(state);
  return (
    <TitleContext.Provider value={state}>
      <SetLangContext.Provider value={dispatch}>
        {children}
      </SetLangContext.Provider>
    </TitleContext.Provider>
  );
};

export default LanguaeProvider;
